﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;

namespace ConsoleApplication1
{
    class Program
    {
        static public Random random = new Random();

        static public int lcs(String str1, String str2)
        {
            int l = str1.Length + 1;
            int c = str2.Length + 1;

            int[,] vet = new int[l, c];

            for (int i = 1; i < l; i++)
            {
                for (int j = 1; j < c; j++)
                {
                    if (str1[i - 1] == str2[j - 1])
                        vet[i, j] = vet[i - 1, j - 1] + 1;
                    else
                        vet[i, j] = Math.Max(vet[i, j - 1], vet[i - 1, j]);
                }
            }
            return vet[l - 1, c - 1];
        }
        static public int[,] lcs2(String str1, String str2)
        {
            int l = str1.Length + 1;
            int c = str2.Length + 1;

            int[,] vet = new int[l, c];

            for (int i = 1; i < l; i++)
            {
                for (int j = 1; j < c; j++)
                {
                    if (str1[i - 1] == str2[j - 1])
                        vet[i, j] = vet[i - 1, j - 1] + 1;
                    else
                        vet[i, j] = Math.Max(vet[i, j - 1], vet[i - 1, j]);
                }
            }
            return vet;
        }

        static public string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }       

        static public int lcsRecursivo(String X, String Y, int tamX, int tamY)
        {
            if (tamX == 0 || tamY == 0)
                return 0;
            if (X[tamX - 1] == Y[tamY - 1])
                return 1 + lcsRecursivo(X, Y, tamX - 1, tamY - 1);
            else
                return max(lcsRecursivo(X, Y, tamX, tamY - 1), lcsRecursivo(X, Y, tamX - 1, tamY));
        }

        static public int max(int a, int b)
        {
            return (a > b) ? a : b;
        }

            static void Main(string[] args)
            {
                while (true)
            {
                int NDinamico = 200001;
                int NRecursivo = 27;
                int nSomatorioFor = 10000;
                string txt = "algoritmo.csv";
                int TEMPO_MEDICAO = 1;
                String str1 = "ABC", str2 = "";

                int numExecucoes;
                double tempoExecucao, limite, resultado = 0;
                double t1, t2;
                Stopwatch relogio = new Stopwatch();

                relogio.Start();

                int op = 1;
                Console.Write("\n\n\n");                    
                Console.Write("1 -    Algoritmo programacao dinamica parametro 1 fixo \n");
                Console.Write("2 -    Algoritmo programacao dinamica parametro 2 fixo \n");
                Console.Write("3 -    Algoritmo programacao dinamica parametro dinamicos \n");

                Console.Write("4 -    Algoritmo recursivo parametro 1 fixo \n");
                Console.Write("5 -    Algoritmo recursivo parametro 2 fixo \n");
                Console.Write("6 -    Algoritmo recursivo parametro dinamicos \n");

                Console.Write("7 -    Deletar arquivo \n");
                Console.Write("8 -    Abrir excel \n");
                

                op = Convert.ToInt32(Console.ReadLine());
                switch (op)
                {
                    case 1:
                        using (StreamWriter writer = new StreamWriter(txt, true))
                        {

                            str1 = RandomString(10);
                            writer.WriteLine(" Opcao 1: programacao dinamica \n");
                            writer.WriteLine(" n; tempoExecucao ;   segundos  (numExecucoes  \n");
                            for (int n = 1; n <= NDinamico; n = nSomatorioFor + n)
                            {
                                numExecucoes = 0;
                                t1 = relogio.ElapsedMilliseconds;
                                limite = t1 + 1000 * TEMPO_MEDICAO;

                                while (relogio.ElapsedMilliseconds < limite)
                                {
                                    resultado += lcs(str1, RandomString(n));
                                    numExecucoes++;
                                }

                                var t22 = relogio.ElapsedMilliseconds;
                                tempoExecucao = (t22 - t1) / 1000 / numExecucoes;

                                writer.WriteLine(n + ";" + tempoExecucao.ToString("0.00000000000000000000000") + ";" + numExecucoes);
                                Console.Write(" n = " + n + "  ; tempoExecucao =  " + tempoExecucao.ToString("0.00000000000000000000000") + " segundos ; (numExecucoes = " + numExecucoes + ")\n");
                            }
                        }
                        break;
                    case 2:
                        using (StreamWriter writer = new StreamWriter(txt, true))
                        {

                            str1 = RandomString(10);
                            writer.WriteLine(" Opcao 1: programacao dinamica \n");
                            writer.WriteLine(" n; tempoExecucao ;   segundos  (numExecucoes  \n");
                            for (int n = 1; n <= NDinamico; n = nSomatorioFor + n)
                            {
                                numExecucoes = 0;
                                t1 = relogio.ElapsedMilliseconds;
                                limite = t1 + 1000 * TEMPO_MEDICAO;

                                while (relogio.ElapsedMilliseconds < limite)
                                {
                                    resultado += lcs(RandomString(n), str1);
                                    numExecucoes++;
                                }

                                var t22 = relogio.ElapsedMilliseconds;
                                tempoExecucao = (t22 - t1) / 1000 / numExecucoes;

                                writer.WriteLine(n + ";" + tempoExecucao.ToString("0.00000000000000000000000") + ";" + numExecucoes);
                                Console.Write(" n = " + n + "  ; tempoExecucao =  " + tempoExecucao.ToString("0.00000000000000000000000") + " segundos ; (numExecucoes = " + numExecucoes + ")\n");
                            }
                        }
                        break;
                    case 3:
                        using (StreamWriter writer = new StreamWriter(txt, true))
                        {
                            str1 = RandomString(10);
                            writer.WriteLine(" Opcao 1: programacao dinamica parametros dinamicos \n");
                            writer.WriteLine(" n; tempoExecucao ;   segundos  (numExecucoes  \n");
                            for (int n = 1; n <= NRecursivo; n++)
                            {
                                numExecucoes = 0;
                                t1 = relogio.ElapsedMilliseconds;
                                limite = t1 + 1000 * TEMPO_MEDICAO;

                                while (relogio.ElapsedMilliseconds < limite)
                                {
                                    resultado += lcs(RandomString(n), RandomString(n));
                                    numExecucoes++;
                                }

                                var t22 = relogio.ElapsedMilliseconds;
                                tempoExecucao = (t22 - t1) / 1000 / numExecucoes;

                                writer.WriteLine(n + ";" + tempoExecucao.ToString("0.00000000000000000000000") + ";" + numExecucoes);
                                Console.Write(" n = " + n + "  ; tempoExecucao =  " + tempoExecucao.ToString("0.00000000000000000000000") + " segundos ; (numExecucoes = " + numExecucoes + ")\n");
                            }
                        }
                        break;
                    case 4:
                        using (StreamWriter writer = new StreamWriter(txt, true))
                        {
                            str1 = RandomString(10);
                            writer.WriteLine("\n\n Opcao 2: Algoritmo recursivo fixo string 1 \n");
                            writer.WriteLine(" n; tempoExecucao ;   segundos  (numExecucoes  \n");
                            for (int n = 1; n <= NRecursivo; n++)
                            {
                                numExecucoes = 0;
                                t1 = Convert.ToDouble(relogio.ElapsedMilliseconds);
                                limite = t1 + 1000 * TEMPO_MEDICAO;

                                while (Convert.ToDouble(relogio.ElapsedMilliseconds) < limite)
                                {
                                    str2 = RandomString(n);
                                    resultado += lcsRecursivo(str1, str2, str1.Length, str2.Length);
                                    numExecucoes++;
                                }

                                var t22 = Convert.ToDouble(relogio.ElapsedMilliseconds);
                                tempoExecucao = (t22 - t1) / 1000 / numExecucoes;
                                
                                writer.WriteLine(n + ";" + tempoExecucao.ToString("0.00000000000000000000000") + ";" + numExecucoes);
                                Console.Write(" n = " + n + "  ; tempoExecucao =  " + tempoExecucao.ToString("0.00000000000000000000000") + " segundos ; (numExecucoes = " + numExecucoes + ")\n");
                            }

                        }
                        break;
                    case 5:
                        using (StreamWriter writer = new StreamWriter(txt, true))
                        {
                            str1 = RandomString(10);
                            writer.WriteLine("\n\n Opcao 2: Algoritmo recursivo fixo string 1 \n");
                            writer.WriteLine(" n; tempoExecucao ;   segundos  (numExecucoes  \n");
                            for (int n = 1; n <= NRecursivo; n++)
                            {
                                numExecucoes = 0;
                                t1 = Convert.ToDouble(relogio.ElapsedMilliseconds);
                                limite = t1 + 1000 * TEMPO_MEDICAO;

                                while (Convert.ToDouble(relogio.ElapsedMilliseconds) < limite)
                                {
                                    str2 = RandomString(n);
                                    resultado += lcsRecursivo(str2, str1, str2.Length, str1.Length);
                                    numExecucoes++;
                                }

                                var t22 = Convert.ToDouble(relogio.ElapsedMilliseconds);
                                tempoExecucao = (t22 - t1) / 1000 / numExecucoes;
                                writer.WriteLine(n + ";" + tempoExecucao.ToString("0.00000000000000000000000") + ";" + numExecucoes);
                                Console.Write(" n = " + n + "  ; tempoExecucao =  " + tempoExecucao.ToString("0.00000000000000000000000") + " segundos ; (numExecucoes = " + numExecucoes + ")\n");
                            }

                        }
                        break;
                    case 6:
                        using (StreamWriter writer = new StreamWriter(txt, true))
                        {
                            str1 = RandomString(10);
                            writer.WriteLine("\n\n Opcao 2: Algoritmo recursivo fixo string 1 \n");
                            writer.WriteLine(" n; tempoExecucao ;   segundos  (numExecucoes  \n");
                            for (int n = 1; n <= 16; n++)
                            {
                                numExecucoes = 0;
                                t1 = Convert.ToDouble(relogio.ElapsedMilliseconds);
                                limite = t1 + 1000 * TEMPO_MEDICAO;

                                while (Convert.ToDouble(relogio.ElapsedMilliseconds) < limite)
                                {
                                    str2 = RandomString(n);
                                    str1 = RandomString(n);
                                    resultado += lcsRecursivo(str2, str1, str2.Length, str1.Length);
                                    numExecucoes++;
                                }

                                var t22 = Convert.ToDouble(relogio.ElapsedMilliseconds);
                                tempoExecucao = (t22 - t1) / 1000 / numExecucoes;
                                writer.WriteLine(n + ";" + tempoExecucao.ToString("0.00000000000000000000000") + ";" + numExecucoes);
                                Console.Write(" n = " + n + "  ; tempoExecucao =  " + tempoExecucao.ToString("0.00000000000000000000000") + " segundos ; (numExecucoes = " + numExecucoes + ")\n");
                            }

                        }
                        break;
                    case 7:
                        File.Delete(txt);
                        break;
                    case 8:
                        Process.Start(txt);
                        break;
                    
                }
            }            
        }
    }
}
